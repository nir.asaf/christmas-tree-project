/* eslint-disable no-trailing-spaces */
/* eslint-disable prefer-destructuring */
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const mongooseConfig = require('./server/config/dbConfig');
const routes = require('./server/routes');

const app = express();
const port = 3000;

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(__dirname+'/public'));
mongoose.Promise = mongooseConfig.promiseLibrary;
mongoose.connect(mongooseConfig.url);

/* GET home page. */
app.get('/', (req, res) => {
	res.render('index', { title: 'Express' });
});

app.use('/api', routes);

app.listen(process.env.PORT || port, () => {
  console.log(`Running on port: ${port}`);
});
