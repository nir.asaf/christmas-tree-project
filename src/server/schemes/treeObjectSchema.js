const mongoose = require('mongoose');

require('mongoose-type-url');

const schema = mongoose.Schema;

// define the schema for our object model
const treeObjectSchema = mongoose.Schema({
  uuid: String,
  user: {
    type: schema.ObjectId,
    ref : 'User',
  },
  symbol: {
    type: String,
    trim: true,
    required: true
  }, // Can be one of those types: [star, gift, toy, bell, cane, sleigh]
  url: {
    type: mongoose.SchemaTypes.Url,
    required: true
  },
  name: String,
  text: String,
  position: {
    x: Number,
    y: Number,
    z: Number,
  }
});

module.exports = mongoose.model('object', treeObjectSchema);
