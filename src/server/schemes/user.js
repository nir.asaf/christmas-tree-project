const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

// define the schema for our user model
const userSchema = mongoose.Schema({
  email: String,
  firstName: String,
  lastName: String,
  password: String,
  updated_at: { type: Date, default: Date.now },
  role: {
    type: String,
    default: 'USER',
  },
});

userSchema.methods.generateHash = (password) => {
  bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validatePassword = password => bcrypt.compareSync(password, this.password);

module.exports = mongoose.model('User', userSchema);
