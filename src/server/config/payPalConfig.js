const paypalAuth = {
  mode: 'sandbox',
  client_id: 'YOUR_CLIENT_ID_HERE',
  client_secret: 'YOUR_CLIENT_SECRET_HERE'
};

module.exports = paypalAuth;
