const paypal = require('paypal-rest-sdk');

const pay = async (req, res) => {
  // create payment object
  const payment = {
    intent: 'authorize',
    payer: {
      payment_method: 'paypal'
    },
    redirect_urls: {
      return_url: 'http://127.0.0.1:3000/success',
      cancel_url: 'http://127.0.0.1:3000/err'
    },
    transactions: [{
      amount: {
        total: 1.00,
        currency: 'USD'
      },
      description: 'Christmas Tree'
    }]
  };

  // call the create Pay method
  createPay(payment).then((transaction) => {
    const id = transaction.id; 
    const links = transaction.links;
    let counter = links.length; 
    while(counter--) {
      if (links[counter].method == 'REDIRECT') {
        // redirect to paypal where user approves the transaction
        return res.redirect(links[counter].href )
      }
    }
  }).catch((err) => { 
    console.log(err); 
    res.redirect('/err');
  });
};

const createPay = (payment) => {
  const promise = new Promise((resolve, reject) => {
    paypal.payment.create(payment, (err, _payment) => {
      if (err) {
        reject(err);
      } else {
        resolve(_payment);
      }
    });
  });
  return promise;
};

module.exports = pay;
