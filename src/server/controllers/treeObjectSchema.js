const TreeObjectSchema = require('../schemes/treeObjectSchema');

export const createObject = async (req, res) => {
  if ( req.body ) {
    try {
      const object = await new TreeObjectSchema({});
    } catch (e) {
      res.status(400).json({'msg': 'Could not create object', error: e});
    }
  }
};

export const getObject = async (req, res) => {
  
}

export const removeObject = async (req, res) => {}

export const editObject = async (req, res) => {}
