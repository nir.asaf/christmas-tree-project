const jwt = require('jwt-simple');
const jwtAuth = require('../config/authConfig');
const User = require('../schemes/user');

/**
 * Create user
 */
const create = async (req, res) => {
  const { email, password } = req.body;
  if (!email || !password) {
    return res.status(400).json({
      msg: 'invalid email/password',
      success: false,
    });
  }
  try {
    const user = await User.findOne({ email });
    if (user) {
      return res.status(400).json({
        msg: 'email exists',
        success: false
      });
    }

    const newUser = User({
      email: req.body.email,
    });

    newUser.password = newUser.generateHash(password);
    await newUser.save();

    res.json({
      success: true,
      newUser,
    });
  } catch (e) {
    res.status(500).json({
      msg: 'Could not create user',
      error: e,
      success: false,
    });
  }
};

const localLogin = async (req, res) => {
  try {
    const payload = {
      id: req.user.id
    };
    const token = jwt.encode(payload, jwtAuth.jwtSecret);
    res.json({
      jwt: token,
      user: req.user,
      success: true,
    });
  } catch (e) {
    res.status(400).json({
      msg: 'Unable to extend token',
      success: false,
    });
  }
};


const removeUser = async (req, res) => {};

const userBuy = async (req, res) => {};

module.exports = {
  create
};
